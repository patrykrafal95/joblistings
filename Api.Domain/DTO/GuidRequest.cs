﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Domain.DTO
{
    public class GuidRequest
    {
        public Guid id { get; set; }
    }
}
